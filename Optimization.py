#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 09:04:06 2019

@author: christoph
"""
import numpy as np


def Optimization(err_function, param,std=1,iterations=5,callback=None,mean=0):
    #print('old parameters:\t'+str(params))
    olderror=err_function(param)
    step=np.random.normal(loc=mean,scale=std)
    k=1
    for it in range(iterations):
            oldpar=param
            param+=steparray
            newerror=err_function(param)
            if callback!=None:
                cbarray=[params[0],params[1],params[2],newerror,k]
                k+=1
                callback(cbarray)
            if newerror<olderror:
                olderror=newerror
                step*=1.2
            else:
                param=oldpar
                step*=-0.7
                
                    
            
           
    #print('new parameters:\t'+str(params)+'\r\r')
    return params, newerror
        
def quadratic_potential(err_function, param,std=1,iterations=1,callback=False,mean=0,return_error=True):
     
    def CalcParabolaVertex(x1, y1, x2, y2, x3, y3):
        denom = (x1 - x2) * (x1 - x3) * (x2 - x3);
        A = (x3 * (y2 - y1) + x2 * (y1 - y3) + x1 * (y3 - y2)) / denom;
        B = (x3*x3 * (y1 - y2) + x2*x2 * (y3 - y1) + x1*x1 * (y2 - y3)) / denom;
        #C = (x2 * x3 * (x2 - x3) * y1 + x3 * x1 * (x3 - x1) * y2 + x1 * x2 * (x1 - x2) * y3) / denom;
        if A==0:
            return 0
        return (-B / (2*A))
             
    
        
    olderror = err_function(param)   
    if callback:
        print('old error:\t'+str(olderror))
    for j in range(iterations):
        step=np.random.normal(loc=mean,scale=std)
        grads=[]
        oldpar=param
        k = 1     
        param+=(k*step)
        newerror=err_function(param)
        grads.append(olderror-newerror)
        if callback:
            print('new error:\t'+str(newerror))
        if newerror<olderror:
            olderror=newerror
            continue
        k = -2        
        param+=(k*step)
        newerror=err_function(param)
        grads.append(olderror-newerror)
        
        if newerror<olderror:
            olderror=newerror
            

            continue

        shift=CalcParabolaVertex(0,0,step,grads[0],-step,grads[1])

        param=oldpar+shift
        newerror=err_function(param)
        if callback:
            print('new error2:\t'+str(newerror))
        if newerror>=olderror:
            param=oldpar
            step*=-0.4
            newerror = olderror

        else:
            olderror=newerror


        
    if return_error:
        return (param,newerror)
    else:
        return param
           
            
