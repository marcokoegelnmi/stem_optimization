from ase import Atoms
from ase.io import read, write
import numpy as np
from Simulation import Simulation
from Optimization import quadratic_potential as qp
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from tqdm import tqdm
import time
import threading

#import matplotlib
#%matplotlib notebook



class Model:
    def __init__(self, ase_object, image,**kwargs):
        self.ase = ase_object
        self.contrast =  kwargs.get('contrast',1.6)
        self.intensities = kwargs.get('intensities',self.ase.get_atomic_numbers()**self.contrast)
        self.intensities /= self.ase.get_atomic_numbers()**self.contrast ## self.intensities only shows deviation from expected intensity
        ## this is the parameter which is referred as the scattering intensity:
        self.total_scattering = self.intensities*self.ase.get_atomic_numbers()**self.contrast 
        self.image = image
        self.imageWidth = self.image.shape[1]
        self.imageHeight = self.image.shape[0]
        self.sim = Simulation(self, shape = self.image.shape, kernelsize = kwargs.get('kernelsize',4))
        self.abkeys = ['EHTFocus','C12_a','C12_b','C21_a','C21_b', 'C23_a', 'C23_b']
        # all paramteres which can be optimized:
        self.kwparams = ['positions','fov', 'blur', 'aberrations','intensities','tilt','translation','scale']
        
        self.fig = plt.figure()
        gs=GridSpec(2,3)
        self.ax1 = self.fig.add_subplot(gs[0,0]) # First row, first column
        self.ax2 = self.fig.add_subplot(gs[0,1]) # First row, second column
        self.ax3 = self.fig.add_subplot(gs[0,2]) # First row, third column
        self.ax4 = self.fig.add_subplot(gs[1,0]) # Second row,fist column
        self.ax5 = self.fig.add_subplot(gs[1,1:]) # Second row, second and third column
        self.rotation = [0,0]
        self.ax = [self.ax1,self.ax2,self.ax3,self.ax4,self.ax5]
        
        

	#self.fig, self.ax  = plt.subplots(2,3,figsize = (8,3), 
#		gridspec_kw={'width_ratios': [1, 1,1,2]})
        self.simulation = np.zeros(self.image.shape)
        self.diffimage = self.image-self.simulation
        self.error = self.calc_error()
        self.error_array = []
        
        self.ax[0].imshow(self.image)
        self.ax[1].imshow(self.simulation)
        self.ax[2].imshow(self.diffimage)        
        self.ax[3].imshow(self.sim.probe)        
        self.ax[4].plot(self.error_array)
        plt.tight_layout()
        
        #self.ax1 = self.ax[0].imshow(self.image)
        #self.ax2 = self.ax[1].imshow(self.simulation)
        #self.ax3 = self.ax[2].imshow(self.diffimage)
        #self.ax4 = self.ax[3].plot(self.error_array)
        ## defines, when to update plot in multi_threding
        self.update_plot_trigger = True
        self.break_PlotThread = False
        #if kwargs.get('start_PlotThread', False):
        #    self.thread_img = threading.Thread(target = self.update_plots,kwargs={'infinite' : True}, daemon=True)
        #    self.thread_img.start()
    
        #self.error = self.calc_error()
        
    def update_scattering(self):
        self.total_scattering = self.intensities*self.ase.get_atomic_numbers()**self.contrast
        return self.total_scattering
    
    def get_relative_intensities(self,exclude_NaNs=True):
        self.update_scattering()
        if exclude_NaNs:
            points = (self.sim.unit_conversion()+0.5).astype(int)
            nans = np.isnan(self.image[points[:,1],points[:,0]])
            
            
            
        ## estimate intensity of lightest element
        lightest_mean = np.mean(self.total_scattering[self.ase.get_atomic_numbers()==np.min(self.ase.get_atomic_numbers())])
       
        if exclude_NaNs:
            return self.total_scattering[np.invert(nans)]/lightest_mean
        else:
            self.total_scattering/lightest_mean
    
    def set_up_simulation(self, convergence_angle, energy, imsize,**kwargs):
        self.sim.convergence_angle = convergence_angle
        self.sim.energy = energy
        self.sim.shape = self.image.shape
        self.sim.canvas_size = int(np.max(self.sim.shape)/2)
        self.sim.method = kwargs.get('method','ADF')
        self.sim.imsize = imsize
        self.sim.imsize_final = imsize
        self.sim.aberrations = kwargs.get('aberrations',
                                          {'EHTFocus': 0, 'C12_a': 0, 'C12_b': 0, 
                                           'C21_a': 0, 'C21_b': 0, 'C23_a': 0,  'C23_b': 0})
        self.sim.blur = kwargs.get('blur',0)
        self.sim.atom0 = kwargs.get('atom0',None)
        self.sim.pixel0 = kwargs.get('pixel0',None)
        self.sim.shift0 = [0,0]
        if self.sim.pixel0!=None and self.sim.atom0!=None:
            self.sim.shift0 = np.array([self.sim.pixel0[1],self.sim.pixel0[0]])-self.sim.unit_conversion()[self.sim.atom0]-1
        self.simulate_image()
        self.calc_error()
        self.update_plot_trigger = True
        
    def set_position(self,x,y, index):
        z = self.ase.get_positions()[2]
        self.ase.positions[index] = [x,y,z]
        
    def simulate_image(self,match_contrast = True):
        self.simulation = self.sim.simulate_image()
        self.simulation[np.isnan(self.image)]=np.nan
        if match_contrast:
            self.match_mean_and_std()
        
        
    def match_mean_and_std(self):
        self.simulation-=np.nanmean(self.simulation)
        self.simulation=self.simulation/np.nanstd(self.simulation)*np.nanstd(self.image)
        self.simulation+=np.nanmean(self.image)
        
    def normalize_intensities(self):
        self.simulate_image(match_contrast=False)
        self.intensities-=np.nanmean(self.simulation)
        self.intensities=self.intensities/np.nanstd(self.simulation)*np.nanstd(self.image)
        self.intensities+=np.nanmean(self.image)
        self.simulate_image(match_contrast=False)
        
    def calc_error(self):
        self.diffimage = self.simulation-self.image
        if np.nanstd(self.simulation)==0 or np.isnan(np.nanstd(self.simulation)):## use worst value for nan image
            self.error = -1
        else:
            self.error = np.nansum((np.nanmean(self.simulation)-self.simulation)*(np.nanmean(self.image)-self.image))/ \
                (np.nanstd(self.simulation)*np.nanstd(self.image)*(self.imageHeight*self.imageWidth-1))
        if np.isnan(self.error):
            self.error=-1 ## use worst value for nan error
        
        
    def simulate_and_update(self):
        self.simulate_image()
        self.calc_error()
        
    
                
    def plotting_thread(self, fig, axe):
        while (True):
            if self.break_PlotThread:
                break
            ## boolean which tells if update is required
            if self.update_plot_trigger:
                if self.error != None:
                    fig.suptitle('correlation: '+str(self.error))
                axe[1].cla()
                axe[1].imshow(self.simulation)
                axe[2].cla()
                axe[2].imshow(self.diffimage)
                axe[3].cla()
                axe[3].imshow(self.sim.probe)
                axe[4].cla()
                axe[4].plot(self.error_array) 

                axe[-1].set_xlabel('iteration')
                axe[-1].set_ylabel('correlation')
                time.sleep(3)
        self.break_PlotThread = False
    
        
    def optimize_model(self,args, iterations = 1):
        

        """
    
        parameters contains all possible parameters for (iterative) optimzation :

        Possible Parameters
        -------------------

        'positions' 
            optimizes projected atomic positions

        'aberrations'
        optimizes aberrations up to 3rd order

        'fov'
        optimizes field of view (corresponding to atomic width)

        'blur'
        
            optimizes blur of image (caused by e.g. source size, vibrations ...)

        'intensities'
        
            optimizes atomic intensities
        """
        for j in tqdm(range(iterations)):
            #print('\nIteration '+str(j+1))
            for arg in args:
                if arg not in self.kwparams:
                    print('WARNING: parameter '+arg+' not known')
                    continue

                #print('Optimizing '+arg+'\t\t\t', end='\r')

                ## setting up variables for set_parameter()
                atomnumber_ = None
                coordinate_ = None
                aberrationkw_ = None     
                iter_ = 1

                if arg == 'aberrations':
                    iter_ = len(self.abkeys) 

                elif arg in ['positions', 'intensities']:
                    iter_ = self.ase.get_global_number_of_atoms() 



                for i in range(iter_):
                    if arg=='aberrations':
                        aberrationkw_ = self.abkeys[i]
                    elif arg in ['positions', 'intensities']:
                        atomnumber_ = i
                    if arg == 'positions' or 'tilt' or 'translation':
                        coordinate_ = 0




                    def errfun(value):  
                        self.set_parameter(arg, value,
                                           atomnumber = atomnumber_, coordinate = coordinate_, 
                                           aberrationkw = aberrationkw_)
                        self.simulate_and_update()                     
                        return -self.error # We minimize the negative correlation


                    olderror = self.error
                    oldpars = self.get_single_parameter(arg,
                                           atomnumber = atomnumber_, coordinate = coordinate_, 
                                           aberrationkw = aberrationkw_)

                    res, fopt=qp(errfun,oldpars, 
                                 std=self.get_variations(arg, aberrationkw = aberrationkw_),callback=False)

                    if fopt>=olderror:
                        self.set_parameter(arg,oldpars,atomnumber = atomnumber_,
                                                   coordinate = coordinate_,aberrationkw = aberrationkw_)
                        self.simulate_and_update()
                        continue
                    #print('new correlation: '+str(self.error))
                    #self.update_plots()
                    self.set_parameter(arg,res,atomnumber = atomnumber_,
                                                   coordinate = coordinate_,aberrationkw = aberrationkw_)
                    self.simulate_and_update()
                    self.error_array.append(self.error)
                    self.update_plot_trigger = True
                    #print('new correlation: ' +str(self.error))
                    
                    if arg == 'positions' or 'tilt' or 'translation':
                        coordinate_ = 1
                        olderror = self.error
                        oldpars = self.get_single_parameter(arg,
                                               atomnumber = atomnumber_, coordinate = coordinate_, 
                                               aberrationkw = aberrationkw_)

                        res, fopt=qp(errfun,oldpars, 
                                     std=self.get_variations(arg, aberrationkw = aberrationkw_),callback=False)

                        if fopt>=olderror:
                            self.set_parameter(arg,oldpars,atomnumber = atomnumber_,
                                                       coordinate = coordinate_,aberrationkw = aberrationkw_)
                            self.simulate_and_update()
                            continue
                        #print('new correlation: '+str(self.error))
                        #self.update_plots()
                        self.set_parameter(arg,res,atomnumber = atomnumber_,
                                                       coordinate = coordinate_,aberrationkw = aberrationkw_)
                        self.simulate_and_update()
                        self.error_array.append(self.error)
                        self.update_plot_trigger = True
                        #print('new correlation: ' +str(self.error))
            #print('\t\t\t\t\t\t', end='\r')
                
          

    '''
    sets values for certain parameters
    
    keyword: see optimze_model for possible keywords
   
   
    value: float
    atomnumber ... integer for parameters 'postions' and 'intensities'
    
    coordinate required for parameter 'positions': 0...x, 1...y
    
    aberrationkw required for parameter 'aberration':
        ['EHTFocus','C12_a','C12_b','C21_a','C21_b', 'C23_a', 'C23_b']
    '''
    def set_parameter(self, keyword, value, **kwargs):
        if keyword == 'blur':
            self.sim.blur = np.abs(value)
        elif keyword == 'fov':
            self.sim.imsize = np.abs(value)
        elif keyword == 'scale':
            self.sim.imsize_final = np.abs(value)
            
        elif keyword == 'convergence_angle':
            self.sim.convergence_angle = value 
        elif keyword == 'aberrations':
            aberrationkw = kwargs.get('aberrationkw',None)
            if aberrationkw not in self.abkeys:
                print('aberration keyword not valid')
                return
            self.sim.aberrations[aberrationkw] = value ## dictionary
        elif keyword == 'positions':
            atomnumber = kwargs.get('atomnumber',None)
            if not (atomnumber>=0 and atomnumber<self.ase.get_global_number_of_atoms()):
                print('atom number not valid')
                return
            coordinate = kwargs.get('coordinate',-1)
            if coordinate not in [0,1]:
                print('invald coordinate')
                return
            self.ase[atomnumber].position[coordinate] = value 
            
            
        elif keyword == 'intensities':
            atomnumber = kwargs.get('atomnumber',None)
            if not (atomnumber>=0 and atomnumber<self.ase.get_global_number_of_atoms()):
                print('atom number not valid')
                return

            self.intensities[atomnumber] = np.abs(value )
        
        elif keyword == 'tilt':
            coordinate = kwargs.get('coordinate',-1)
            if coordinate not in [0,1]:
                print('invald coordinate')
                return
            if coordinate ==0:
                self.ase.rotate('x', value-self.rotation[coordinate] )   
            if coordinate ==1:
                self.ase.rotate('y', value-self.rotation[coordinate] ) 
            self.rotation[coordinate]  = value
            
        elif keyword == 'translation':
            coordinate = kwargs.get('coordinate',-1)
            if coordinate not in [0,1]:
                print('invald coordinate')
                return
            self.sim.shift0[coordinate] = value

        else:
            print('keyword not understood')
            
            
    def get_parameters(self):
        return {'blur':self.sim.blur,'fov':self.sim.imsize,'aberrations':self.sim.aberrations,
                'positions': self.ase.get_positions()[:,:2], 'intensities':self.update_scattering()}
    
    def get_single_parameter(self, keyword,**kwargs):
        if keyword == 'blur':
            return self.sim.blur
        elif keyword == 'fov':
            return self.sim.imsize  
        elif keyword == 'scale':
            return self.sim.imsize_final
        elif keyword == 'convergence_angle':
            return self.sim.convergence_angle 
        elif keyword == 'aberrations':
            aberrationkw = kwargs.get('aberrationkw',None)
            if aberrationkw not in self.abkeys:
                print('aberration keyword not valid')
                return
            return self.sim.aberrations[aberrationkw]
        elif keyword == 'positions':
            atomnumber = kwargs.get('atomnumber',None)
            if not (atomnumber>=0 and atomnumber<self.ase.get_global_number_of_atoms()):
                print('atom number not valid')
                return
            coordinate = kwargs.get('coordinate',-1)
            if coordinate not in [0,1]:
                print('invald coordinate')
                return
            return self.ase[atomnumber].position[coordinate] 
            
            
        elif keyword == 'intensities':
            atomnumber = kwargs.get('atomnumber',None)
            
            if not (atomnumber>=0 and atomnumber<self.ase.get_global_number_of_atoms()):
                print('atom number not valid')
                return

            return self.intensities[atomnumber] 
        
        elif keyword == 'tilt':
            coordinate = kwargs.get('coordinate',-1)
            if coordinate not in [0,1]:
                print('invald coordinate')
                return
            return self.rotation[coordinate]
        
        elif keyword == 'translation':
            coordinate = kwargs.get('coordinate',-1)
            if coordinate not in [0,1]:
                print('invald coordinate')
                return
            return self.sim.shift0[coordinate]
            
        else:
            print('keyword not understood')
            return




    def get_variations(self, arg, aberrationkw = None):
        ret = 0
        if arg == 'aberrations':
            if aberrationkw not in self.abkeys:
                print('invalid aberration keyword')
            
            if aberrationkw in ['EHTFocus','C12_a','C12_b','blur','fov']:
                return 1 
            else:
                return 50 
        elif arg in ['intensities','positions','scale']:
            return  0.05
        elif arg in ['blur','tilt','translation']:
            return 0.5
        elif arg in ['fov']:
            return 0.1
        else:
            print('invalid keyword')
            return None
        




    
