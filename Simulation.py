import numpy as np
from scipy.signal import fftconvolve
from scipy.ndimage.filters import gaussian_filter

class Simulation():
    def __init__(self,model, **kwargs):
        self.model = model
        self.method = kwargs.get('method','ADF')
        self.aberrations = kwargs.get('aberrations',{})
        self.convergence_angle = kwargs.get('convergence_angle')
        self.shape = kwargs.get('shape')
        self.imsize = kwargs.get('imsize')
        ## this parameter defines to pixel postions in the image and therefore should not change
        self.imsize_final = kwargs.get('imsize') 
        self.background = kwargs.get('background',0)
        self.kernelsize = kwargs.get('kernelsize',2)
        if self.shape!=None:
            self.canvas_size = int(np.max(self.shape)/self.kernelsize)
            self.potential = np.zeros((self.shape[1]+self.canvas_size-1,
                                       self.shape[0]+self.canvas_size-1))
            self.simulation = np.zeros(self.shape)
            self.probe = np.zeros((int(self.shape[1]/self.kernelsize),int(self.shape[0]/self.kernelsize)))
        


    """
    
    kwargs contains all possible values for the correctors :
        These are all lens aberrations up to threefold astigmatism. 
        
    Possible Parameters
    -------------------

    aberrations : dictionary
        e.g. {'EHTFocus': 0, 'C12_a': 0, 'C12_b': 0, 'C21_a': 0, 'C21_b': 0, 'C23_a': 0,  'C23_b': 0} (all in nm)

    atoms: Model object (ase + intensities)
        
    convergence_angle: float
        STEM convergence angle in rad (e.g. 0.025)
        
    energy: float
        electron energy in eV (e.g. 60000)   
    
    shape: tuple of ints
        shape of resulting image (e.g. (512,512))
        
    imsize: float
        size of image in nm, field of view (e.g. 8)


    Example call of simulate_image:
    ------------------------------
    
    sim = Simulation
    result = sim.simulate_image()

    """
    
    ## unit conversion of the atomic positions, from angstrom to pixel
    def unit_conversion(self):
        return self.model.ase.get_positions()[:,:2]/((self.imsize_final*10)/self.shape[1])
            
        
        
    def calc_potential(self):
        self.potential.fill(self.background)
        pxpositions = self.unit_conversion()
        px, py = pxpositions[:,0], pxpositions[:,1]
        #(px.astype(int)>0)*(px.astype(int)>self.shape[1])*(py.astype(int)>0)*(py.astype(int)>self.shape[0])
        #px = px[]
        #py = py[]
        px+=self.shift0[0]
        py+=self.shift0[1]
        intensity = self.model.update_scattering()
        '''self.potential[tuple([int(self.canvas_size/self.kernelsize)+py.astype(int),
                              int(self.canvas_size/self.kernelsize)+px.astype(int)])]+= \
                    (1-(px-px.astype(int)))*(1-(py-py.astype(int)))*intensity
        
        self.potential[tuple([int(self.canvas_size/self.kernelsize)+py.astype(int),
                              int(self.canvas_size/self.kernelsize)+px.astype(int)+1])]+= \
                    (px-px.astype(int))*(1-(py-py.astype(int)))*intensity
        
        self.potential[tuple([int(self.canvas_size/self.kernelsize)+(py.astype(int)+1),
                              int(self.canvas_size/self.kernelsize)+px.astype(int)+1])]+= \
                    (px-px.astype(int))*(py-py.astype(int))*intensity
        
        self.potential[tuple([int(self.canvas_size/self.kernelsize)+(py.astype(int)+1),
                              int(self.canvas_size/self.kernelsize)+px.astype(int)])]+= \
                    (1-(px-px.astype(int)))*(py-py.astype(int))*intensity
        '''
        np.add.at(self.potential,tuple([int(self.canvas_size/self.kernelsize)+py.astype(int),
                              int(self.canvas_size/self.kernelsize)+px.astype(int)]),
                    (1-(px-px.astype(int)))*(1-(py-py.astype(int)))*intensity)
        
        np.add.at(self.potential,tuple([int(self.canvas_size/self.kernelsize)+py.astype(int),
                              int(self.canvas_size/self.kernelsize)+px.astype(int)+1]),
                    (px-px.astype(int))*(1-(py-py.astype(int)))*intensity)
        
        np.add.at(self.potential,tuple([int(self.canvas_size/self.kernelsize)+(py.astype(int)+1),
                              int(self.canvas_size/self.kernelsize)+px.astype(int)+1]),
                    (px-px.astype(int))*(py-py.astype(int))*intensity)
        
        np.add.at(self.potential,tuple([int(self.canvas_size/self.kernelsize)+(py.astype(int)+1),
                              int(self.canvas_size/self.kernelsize)+px.astype(int)]),
                    (1-(px-px.astype(int)))*(py-py.astype(int))*intensity)
            

        
                
    def calc_potential2(self):
        self.potential2 = np.zeros((self.shape[1], self.shape[0]))
        self.potential2.fill(self.background)
        pxpositions = self.unit_conversion()
        px, py = pxpositions[:,0], pxpositions[:,1]
        #(px.astype(int)>0)*(px.astype(int)>self.shape[1])*(py.astype(int)>0)*(py.astype(int)>self.shape[0])
        #px = px[]
        #py = py[]
        px+=self.shift0
        py+=self.shift0
        intensity = self.model.update_scattering()
        self.potential2[tuple([py.astype(int),px.astype(int)])]+= \
                    (1-(px-px.astype(int)))*(1-(py-py.astype(int)))*intensity
        
        self.potential2[tuple([py.astype(int),px.astype(int)+1])]+= \
                    (px-px.astype(int))*(1-(py-py.astype(int)))*intensity
        
        self.potential2[tuple([(py.astype(int)+1),px.astype(int)+1])]+= \
                    (px-px.astype(int))*(py-py.astype(int))*intensity
        
        self.potential2[tuple([(py.astype(int)+1),px.astype(int)])]+= \
                    (1-(px-px.astype(int)))*(py-py.astype(int))*intensity
                
    def get_SSBkernel(self,radiusim,aperturesize):
        radius_calibrated = (radiusim/aperturesize)
        w1 = radius_calibrated.copy()
        w1[w1>1]=0
        w2 = radius_calibrated.copy()
        w2[(w2<=1)+(w2>=2)]=2
        r1 = 4/np.pi*(np.arccos(w1/2)-np.arccos(w1)+w1*np.sqrt(1-w1**2)-w1/2*np.sqrt(1-(w1/2)**2))
        r2 = 4/np.pi*(np.arccos(w2/2)-w2/2*np.sqrt(1-(w2/2)**2))
        return (radius_calibrated,r1+r2)
    
        
    def simulate_image(self,debug=False):
        assert self.convergence_angle is not None, 'you have to define the convergence angle'
        assert self.method in ['SSB','ADF','custom'], 'method has to be either ADF or SSB'
        assert self.shape is not None, 'you have to set up the image pixel size'
        assert self.energy is not None, 'you have to define the energy'
        assert self.imsize is not None, 'you have to define the field of view'    
        # Create x and y coordinates such that resulting beam has the same scale as the image.
        # The size of the kernel which is used for image convolution is chosen to be "1/kernelsize"
        # of the image size (in pixels)
        
        kernelpixel = int(self.shape[0]/self.kernelsize)
        
        
        #if self.delta_graphene is None:
                
        impix = self.shape[0]+kernelpixel-1
        imsize = impix/self.shape[0]*self.imsize
        self.wavelength = 1.23/np.sqrt(self.energy*(1+9.78e-7*self.energy))
        aperturesize = (self.convergence_angle/self.kernelsize)*self.imsize/self.wavelength
        
        frequencies = np.matrix(np.fft.fftshift(np.fft.fftfreq(kernelpixel, self.imsize/self.shape[0])))
        x = np.array(np.tile(frequencies, np.size(frequencies)).reshape((kernelpixel,kernelpixel)))
        y = np.array(np.tile(frequencies.T, np.size(frequencies)).reshape((kernelpixel,kernelpixel)))

        # compute aberration function up to threefold astigmatism
        # formula taken from "Advanced Computing in Electron Microscopy",
        # Earl J. Kirkland, 2nd edition, 2010, p. 18
        # wavelength for 60 keV electrons: 4.87e-3 nm
        if self.method == 'ADF':
            raw_kernel = (-self.aberrations.get('EHTFocus', 0) * (x**2 + y**2) +
    
                      np.sqrt(self.aberrations.get('C12_a', 0)**2 + self.aberrations.get('C12_b', 0)**2) *
                      (x**2 + y**2) * np.cos(2 * (np.arctan2(y,x) -
                                             np.arctan2(self.aberrations.get('C12_b', 0),
                                                        self.aberrations.get('C12_a', 0)))) +
                      (2.0/3.0) *
                      np.sqrt(self.aberrations.get('C21_a', 0)**2 + self.aberrations.get('C21_b', 0)**2) *
                      self.wavelength *
                      np.sqrt(x**2 + y**2)**3 * np.cos(np.arctan2(y,x) -
                                                       np.arctan2(self.aberrations.get('C21_b', 0),
                                                                  self.aberrations.get('C21_a', 0))) +
                      (2.0/3.0) *
                      np.sqrt(self.aberrations.get('C23_a', 0)**2 + self.aberrations.get('C23_b', 0)**2) *
                      self.wavelength *
                      np.sqrt(x**2 + y**2)**3 * np.cos(3 * (np.arctan2(y,x) -
                                                       np.arctan2(self.aberrations.get('C23_b', 0),
                                                                  self.aberrations.get('C23_a', 0))))) * \
                      np.pi * self.wavelength
    
            kernel = np.cos(raw_kernel)+1j*np.sin(raw_kernel)
            aperture = np.zeros(kernel.shape)
            # Calculate size of X mrad aperture in k-space for Y eV electrons
            
            
            # "Apply" aperture
            center = tuple((np.array(kernel.shape)/2).astype('int'))
            radius = int(np.rint(aperturesize))
            color = 1
            subarray = aperture[center[0]-radius:center[0]+radius+1, center[1]-radius:center[1]+radius+1]
            y_, x_ = np.mgrid[-radius:radius+1, -radius:radius+1]
            distances = np.sqrt(x_**2+y_**2)
            if subarray.shape != distances.shape:
                print('ERROR: field of view too large?')
                return np.ones(self.shape)*np.nan
            subarray[distances <= radius] = color
    
            kernel *= aperture
            kernel_rs = kernel.copy()
            kernel = np.abs(np.fft.fftshift(np.fft.ifft2(np.fft.fftshift(kernel))))**2
            kernel /= np.sum(kernel)
            self.probe = kernel.copy()
        elif self.method == 'SSB':
            #frequencies = np.matrix(np.fft.fftshift(np.fft.fftfreq(self.shape[0], self.imsize/self.shape[0])))
           # x = np.array(np.tile(frequencies, np.size(frequencies)).reshape((self.shape[0],self.shape[0])))
            #y = np.array(np.tile(frequencies.T, np.size(frequencies)).reshape((self.shape[0],self.shape[0])))
            kernel = self.get_SSBkernel(np.sqrt(x**2 + y**2),aperturesize)[1]
            #kernel /= np.sum(kernel)
            #kernel = np.abs(np.fft.fftshift(np.fft.ifft2(np.fft.fftshift(kernel))))
            self.calc_potential()
            #im_reci =  np.fft.fftshift(np.fft.fft2(np.fft.fftshift(self.potential2)))
            #dc = im_reci[int(self.shape[0]/2),int(self.shape[0]/2)]
            #im_reci*=kernel
            #im_reci[int(self.shape[0]/2),int(self.shape[0]/2)] =0
            self.kernel = kernel.copy()
            probe = np.fft.fftshift(np.fft.ifft2(np.fft.fftshift(kernel)))#**2
            p = np.ones(probe.shape)
            p[np.angle(probe)==np.pi] = -1
            self.probe=np.abs(probe)*p
            #self.probe-=np.min(self.probe)
            self.probe/=np.max(self.probe)
            self.simulation = fftconvolve(self.potential, self.probe, mode='valid')
            self.simulation = gaussian_filter(self.simulation,abs(self.blur))
            return_image = self.simulation
            
            '''
            self.atom = np.zeros(kernel.shape)
            self.atom[int(self.atom.shape[0]/2),int(self.atom.shape[0]/2)] = 1 
            self.atom = fftconvolve(self.atom, self.probe)
            self.probe = self.atom'''
            if debug:
                return return_image, kernel, np.fft.fftshift(np.fft.ifft2(np.fft.fftshift(kernel)))
            
            return return_image
    
            
            #return self.simulation
        elif self.method == 'custom':
            assert self.custom_kernel is not None, 'you have to provide a custom kernel' 
            self.calc_potential()
            self.probe = self.custom_kernel
            self.simulation = fftconvolve(self.potential, self.probe, mode='valid')
            self.simulation = gaussian_filter(self.simulation,abs(self.blur))
            return_image = self.simulation               
            return return_image

            
        
       
        #im = cv2.filter2D(im, -1, kernel)
        '''if self.frame_parameters.get('pixeltime', 0) < 0:
            multiplicator = 1
        else:
            multiplicator = self.frame_parameters.get('pixeltime', 0)*100+1
        if self.frame_parameters.get('pixeltime', 0) >= 0:
            im = np.random.poisson(lam=im.flatten(), size=np.size(im)).astype(im.dtype)

        if debug_mode:
            return_image = (im.reshape(self.shape).astype('float32'), kernel)
        else:
            return_image = im.reshape(self.shape).astype('float32')
        '''

        #print(self.aberrations)
        # update  potential
        self.calc_potential()
        im = fftconvolve((self.potential), self.probe, mode='valid')
        return_image = im.reshape(self.shape).astype('float32')            
        return_image = gaussian_filter(return_image,abs(self.blur))
        self.simulation = return_image
        return return_image
    
    
   
    
    def get_parameters(self):
        return {'blur':self.blur,'fov':self.imsize,'aberrations':self.aberrations,'positions':
                self.model.ase.get_positions()[:,:2], 'intensities':self.model.update_scattering()}
    

         
         
        
         
        
    


